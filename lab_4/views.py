from django.shortcuts import render
from .forms import ScheduleForm
from .models import Schedule
from django.http import HttpResponseRedirect

response = {}

# Test
def mainHome(request):
 	return render(request,'pepew3.html')

def profile(request):
	return render(request, 'pepew3_2.html')

def register(request):
	return render(request, 'pepew3_3.html')

def schedule_form(request):
	response['form'] = ScheduleForm
	return render(request, "ScheduleForm.html", response)

def display_form(request):	
	form = ScheduleForm(request.POST or None)
	if(request.method == "POST" and form.is_valid()):
		response['hour_min'] = request.POST.get('hour_min')
		response['activity'] = request.POST.get('activity')
		response['place'] = request.POST.get('place')
		response['category'] = request.POST.get('category')

		Schedules = Schedule(hour_min=response['hour_min'],
		 					 activity=response['activity'], place=response['place'], category=response['category'])

		
		Schedules.save()

		response['form'] = form

		Schedules = Schedule.objects.all()
		response['Schedules'] = Schedules
		return render(request, 'ScheduleList.html' , response)
	
	else:
		return HttpResponseRedirect('/')

def schedule_list(request):
	Schedules = Schedule.objects.all()
	response['Schedules'] = Schedules
	return render(request, 'ScheduleList.html' , response)

def remove_schedule(request):
	Schedules = Schedule.objects.all().delete()
	return render(request, 'ScheduleList.html', {})